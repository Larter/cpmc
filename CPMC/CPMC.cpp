#include "CPMC.h"
#include "CriticalPath.h"
#include <algorithm>
#include <queue>
std::pair<int, int> CPMC(std::vector<Vertex>& vertices, int maxBudget)
{
	FillCriticalPathsValues(vertices);

	int prevCmax = getCMaxValueFromFilledData(vertices);
	int budgetLeft = maxBudget;

	for(;;)
	{

		FillCriticalPathsValues(vertices);
		FillCriticalPathPower(vertices);

		int bestIndex = -1;

		for (size_t i = 0; i < vertices.size(); i++)
		{
			if (vertices[i].critical_path_value > 0
				&& vertices[i].value > vertices[i].min_value
				&& vertices[i].change_cost < budgetLeft)
			{
				bestIndex = i;
				break;
			}
		}

		if (bestIndex == -1)
			break;

		for (size_t i = bestIndex + 1; i < vertices.size(); i++)
		{
			if (vertices[i].value > vertices[i].min_value
				&& vertices[i].change_cost < budgetLeft)
			{
				if (vertices[i].critical_path_value > vertices[bestIndex].critical_path_value)
				{
						bestIndex = i;
				}
				else if (vertices[i].critical_path_value == vertices[bestIndex].critical_path_value
					&& vertices[i].change_cost < vertices[bestIndex].change_cost)
				{
						bestIndex = i;
				}
			}
		}

		budgetLeft -= vertices[bestIndex].change_cost;
		vertices[bestIndex].value --;

		FillCriticalPathsValues(vertices);
		int tempCmax = getCMaxValueFromFilledData(vertices);
	}



	return{ getCMaxValueFromFilledData(vertices), maxBudget - budgetLeft };
}
