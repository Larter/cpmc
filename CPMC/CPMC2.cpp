#include "CPMC.h"
#include "CriticalPath.h"
#include <algorithm>
#include <queue>
#include <iostream>

struct Option
{
	int costForOne;
	std::vector<int> indexesToChange;

};

Option getBestPossibleOption(const std::vector<Option> & options)
{
	if (options.empty())
	{
		return Option( { -1, {} });
	}
	auto bestOption = options[0];

	for (size_t i = 1; i < options.size(); i++)
		if (options[i].costForOne <= bestOption.costForOne)
			bestOption = options[i];

	return bestOption;
}

Option getMinimumOptionForIndex(std::vector<Vertex>& vertices, int index)
{
	std::vector < Option > possibleOptions;

	if (vertices[index].value > vertices[index].min_value)
		possibleOptions.push_back({ vertices[index].change_cost, {index} });

	Option secondOption;
	secondOption.costForOne = 0;
	for(auto prevIndex : vertices[index].vertexesBeforeThis)
		if (vertices[prevIndex].early_finish == vertices[index].early_start)
		{
			auto bestOption = getMinimumOptionForIndex(vertices, prevIndex);
			if (bestOption.costForOne == -1)
			{
				secondOption.costForOne = -1;
				break;
			}
			else
			{
				secondOption.costForOne += bestOption.costForOne;
				secondOption.indexesToChange.insert(secondOption.indexesToChange.end(), bestOption.indexesToChange.begin(), bestOption.indexesToChange.end());
			}

		}

	if (secondOption.costForOne > 0)
		possibleOptions.push_back(secondOption);

	return getBestPossibleOption(possibleOptions);
}

Option getMinimumIndexesToChangeCMax(std::vector<Vertex>& vertices)
{
	int cmax = getCMaxValueFromFilledData(vertices);
	std::vector < Option > possibleOptions;
	
	for (size_t i = 0; i < vertices.size(); i++)
	{
		if (vertices[i].early_finish == cmax)
		{
			auto possibleOption = getMinimumOptionForIndex(vertices, i);
			if (possibleOption.costForOne != -1)
				possibleOptions.push_back(possibleOption);
		}
	}

	return getBestPossibleOption(possibleOptions);

}

std::pair<int, int> CPMC2(std::vector<Vertex>& vertices, int maxBudget)
{
	int budgetLeft = maxBudget;

	for (;;)
	{
		FillCriticalPathsValues(vertices);

		auto bestOption = getMinimumIndexesToChangeCMax(vertices);

		if (bestOption.costForOne > budgetLeft || bestOption.costForOne == -1)
			break;

		int changeValue = 1;
		for (auto index : bestOption.indexesToChange)
		{
			vertices[index].value -= changeValue;
			budgetLeft -= changeValue * vertices[index].change_cost;
			if (vertices[index].value < vertices[index].min_value || budgetLeft < 0)
				std::cerr << "ZLEEEEE" << std::endl; //error print for mistakes in code
		}

	}



	return{ getCMaxValueFromFilledData(vertices), maxBudget - budgetLeft };
}