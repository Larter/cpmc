#include <algorithm>
#include "CriticalPath.h"


int getCMaxIndexFromFilledData(std::vector<Vertex> & vertices)
{
	int endIndex = 0;
	for (size_t i = 0; i < vertices.size(); i++)
	{
		if (vertices[i].early_finish > vertices[endIndex].early_finish)
			endIndex = i;
	}
	return endIndex;
}

int getCMaxValueFromFilledData(std::vector<Vertex>& vertices)
{
	return vertices[getCMaxIndexFromFilledData(vertices)].early_finish;
}


void FillCriticalPathsValues(std::vector<Vertex>& vertices)
{
	std::vector<int> readyToServe;

	for (auto& vertex : vertices)
		vertex.resetVertexCountingParameters();

	for (size_t i = 0; i < vertices.size(); i++)
		if (vertices[i].vertexesBeforeThisLeft == 0)
			readyToServe.push_back(i);

	int max = 0;
	for (size_t i = 0; i < readyToServe.size(); i++)
	{
		vertices[readyToServe[i]].early_finish =
			vertices[readyToServe[i]].early_start + vertices[readyToServe[i]].value;

		if (vertices[readyToServe[i]].early_finish> max)
		{
			max = vertices[readyToServe[i]].early_finish;
		}

		for (size_t j = 0; j < vertices[readyToServe[i]].vertexesAfterThis.size(); ++j)
		{
			int folInd = vertices[readyToServe[i]].vertexesAfterThis[j];
			vertices[folInd].early_start = std::max(vertices[folInd].early_start,
				vertices[readyToServe[i]].early_finish);
			vertices[folInd].vertexesBeforeThisLeft--;

			if (vertices[folInd].vertexesBeforeThisLeft == 0)
			{
				readyToServe.push_back(folInd);
			}
		}
	}
	readyToServe.clear();


	for (size_t i = 0; i < vertices.size(); i++)
	{
		if (vertices[i].vertexesAfterThisLeft == 0)
		{
			readyToServe.push_back(i);
		}
		vertices[i].latest_finish = max;
	}

	for (size_t i = 0; i < readyToServe.size(); i++) {

		vertices[readyToServe[i]].latest_start =
			vertices[readyToServe[i]].latest_finish - vertices[readyToServe[i]].value;

		for (size_t j = 0; j < vertices[readyToServe[i]].vertexesBeforeThis.size(); ++j) {

			int prevInd = vertices[readyToServe[i]].vertexesBeforeThis[j];
			vertices[prevInd].latest_finish =
				std::min(vertices[prevInd].latest_finish,
					vertices[readyToServe[i]].latest_start);
			vertices[prevInd].vertexesAfterThisLeft--;

			if (vertices[prevInd].vertexesAfterThisLeft == 0)
			{
				readyToServe.push_back(prevInd);
			}
		}
	}
}

int doCriticalPathFollowFor(Vertex & vertex, std::vector<Vertex>& vertices)
{
	for (auto& prevIndex : vertex.vertexesBeforeThis)
		if (vertices[prevIndex].early_finish == vertex.early_start)
			vertex.critical_path_value += doCriticalPathFollowFor(vertices[prevIndex], vertices);

    vertex.critical_path_value = vertex.critical_path_value ?  vertex.critical_path_value : 1;

	return vertex.critical_path_value;
}

void FillCriticalPathPower(std::vector<Vertex>& vertices)
{
	int cmax = getCMaxValueFromFilledData(vertices);
	for (auto& vertex : vertices)
		if (vertex.early_finish == cmax)
		{
			vertex.critical_path_value = doCriticalPathFollowFor(vertex, vertices);
			vertex.critical_path_value = vertex.critical_path_value ? vertex.critical_path_value : 1;
		}

}



std::vector<int> getCP(std::vector<Vertex> & vertices)
{
	std::vector<int> cpm;

	int temp = getCMaxIndexFromFilledData(vertices);

	cpm.push_back(temp);

	while (vertices[temp].early_start != 0)
	{
		int bestInd = vertices[temp].vertexesBeforeThis[0];

		for (size_t i = 0; i < vertices[temp].vertexesBeforeThis.size(); ++i)
		{
			if (vertices[bestInd].early_finish < vertices[vertices[temp].vertexesBeforeThis[i]].early_finish)
			{
				bestInd = vertices[temp].vertexesBeforeThis[i];
			}
		}
		temp = bestInd;
		cpm.push_back(temp);
	}

	std::reverse(cpm.begin(), cpm.end());

	return cpm;
}