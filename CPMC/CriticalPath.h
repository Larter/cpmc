#pragma once
#include "Vertex.h"
#include <vector>
void FillCriticalPathsValues(std::vector<Vertex>& vertices);

void FillCriticalPathPower(std::vector<Vertex>& vertices);

int getCMaxIndexFromFilledData(std::vector<Vertex> & vertices);
int getCMaxValueFromFilledData(std::vector<Vertex> & vertices);


std::vector<int> getCP(std::vector<Vertex> & vertices);