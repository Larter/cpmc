#include "PSO.h"
#include "CPMC.h"
#include "CriticalPath.h"
#include <random>
#include <algorithm>
#include <iostream>

const size_t epochs_count = 1000;
const size_t population_size = 30;


struct ResultContainer
{
	int cmax;
	int budget;
};


bool operator<(const ResultContainer& l, const ResultContainer r)
{
	if (l.cmax == r.cmax)
		return l.budget < r.budget;
	return l.cmax < r.cmax;
}

struct IndividualCopy
{
	std::vector<Vertex> vertices;
	ResultContainer result;
};


struct Individual
{
	std::vector<Vertex> vertices;
	IndividualCopy pBest;
	std::vector<int> velocity_left;
	std::vector<int> velocities;
	int currentBudget;
};

Individual generateIndividual(std::vector<Vertex>& v, int maxBudget)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	Individual individual;
	individual.vertices = v;
	individual.velocity_left.resize(v.size());
	individual.velocities.resize(v.size());
	int budgetSpent = 0;
	for (auto& vertex : individual.vertices)
	{
		std::uniform_int_distribution<> dis(vertex.min_value, vertex.max_value);
		vertex.value = dis(gen);
		budgetSpent += (vertex.max_value - vertex.value)*vertex.change_cost;
	}

	std::uniform_int_distribution<> distribution_index(0, v.size() - 1);
	while (budgetSpent > maxBudget)
	{
		int index = distribution_index(gen);
		if (individual.vertices[index].value < individual.vertices[index].max_value)
		{
			individual.vertices[index].value++;
			budgetSpent -= individual.vertices[index].change_cost;
		}
	}

	FillCriticalPathsValues(individual.vertices);
	individual.pBest.result.cmax = getCMaxValueFromFilledData(individual.vertices);
	individual.pBest.result.budget = budgetSpent;
	individual.pBest.vertices = individual.vertices;
	individual.currentBudget = budgetSpent;
	return individual;
}

std::vector<Individual> prepareData(std::vector<Vertex>& v, int maxBudget)
{


	std::vector<Individual> population(population_size);

	for (auto & individual : population)
	{
		individual = generateIndividual(v, maxBudget);
	}


	return population;


}

std::pair<int, int> PSOAlgorithm(std::vector<Vertex>& v, int maxBudget)
{

	std::random_device rd;
	std::mt19937 gen(rd());


	auto population = prepareData(v, maxBudget);

	IndividualCopy gBest;
	gBest.vertices = v;
	auto res =CPMC(gBest.vertices, maxBudget);
	gBest.result.cmax = res.first;
	gBest.result.budget = res.second;


	int maxSpeed = 0;


	std::uniform_int_distribution<> distribution_index_population(0, population.size() - 1);

	int last_epoch_until_changed =0 ;

	int epoch_print_counter = 0;

	std::vector<int> indexes(v.size());

	for (size_t i = 0; i < indexes.size(); i++)
	{
		indexes[i] = i;
	}

	for (size_t epoch = 0; epoch < epochs_count; epoch++)
	{
		last_epoch_until_changed++;
		
		if(last_epoch_until_changed>population_size/200)
			population = prepareData(v, maxBudget);

		epoch_print_counter++;
		if (epoch_print_counter == epochs_count / 100)
		{
			//std::cout << "% epok skonczonych :" << (epoch * 100) / epochs_count << std::endl;
			//std::cout << "Najlepszy do tej pory : cmax = " << gBest.result.cmax << " budget : " << gBest.result.budget << std::endl;
			epoch_print_counter = 0;
		}

		const int velocity_boarder_to_change = 5;


		std::uniform_int_distribution<> velocity_randomize(-5, 20);

		int velocity_abs_sum = 0;
		int tempVelocity = 0;


		for (auto & individual : population)
		{
			///Count velocities

			velocity_abs_sum = 0;
			for (size_t i = 0; i < v.size(); i++)
			{
				tempVelocity = -(3 * individual.vertices[i].value + velocity_randomize(gen) - 2* individual.pBest.vertices[i].value - gBest.vertices[i].value);

				if (tempVelocity > velocity_boarder_to_change)
					individual.velocities[i]++;
				else if (tempVelocity < -velocity_boarder_to_change)
					individual.velocities[i]--;

				velocity_abs_sum += std::abs(individual.velocities[i]);
				individual.velocity_left[i] = individual.velocities[i];
			}

			if (velocity_abs_sum < 2)
				individual = generateIndividual(v, maxBudget);

			int budgetSpent = 0;

			int tempMaxBudget = maxBudget;


			bool somethingChanged = true;
			while (somethingChanged)
			{
				std::random_shuffle(indexes.begin(), indexes.end());
				somethingChanged = false;
				for (size_t index = 0; index < v.size(); index++)
				{
					int i = indexes[index];
					if (individual.velocity_left[i] < 0
						&& individual.vertices[i].value > individual.vertices[i].min_value
						&& individual.currentBudget+ budgetSpent + v[i].change_cost <= tempMaxBudget)
					{
						somethingChanged = true;
						individual.vertices[i].value--;
						individual.velocity_left[i]++;
						budgetSpent += v[i].change_cost;
					}
					else if (individual.velocity_left[i] > 0
						&& individual.vertices[i].value < individual.vertices[i].max_value
						&& individual.currentBudget+ budgetSpent - v[i].change_cost >= 0)
					{
						somethingChanged = true;
						individual.vertices[i].value++;
						individual.velocity_left[i] --;
						budgetSpent -= v[i].change_cost;
					}
				}
			}

			
			
			individual.currentBudget = individual.currentBudget + budgetSpent;
			FillCriticalPathsValues(individual.vertices);
			ResultContainer fitness = { getCMaxValueFromFilledData(individual.vertices), individual.currentBudget };
			if (fitness < individual.pBest.result)
			{
				individual.pBest.result =fitness;
				individual.pBest.vertices = individual.vertices;
			}
		}

		auto gBest_temp = std::min_element(population.begin(), population.end(), [&](const Individual & l, const Individual & r)
		{
			return l.pBest.result < r.pBest.result;
		})->pBest;

		if (gBest_temp.result < gBest.result)
		{
			last_epoch_until_changed = 0;
			gBest = gBest_temp;
			//std::cout << "RESULT HAS CHANGED to cmax = " << gBest.result.cmax << " budget : " << gBest.result.budget << std::endl;
		}
	}


	v = gBest.vertices;
	return{gBest.result.cmax, gBest.result.budget };
}
