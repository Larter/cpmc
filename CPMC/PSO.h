#pragma once

#include "Vertex.h"

std::pair<int, int> PSOAlgorithm(std::vector<Vertex>&, int maxBudget);