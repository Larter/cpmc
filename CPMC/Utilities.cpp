#include "Utilities.h"
#include <fstream>
#include <iostream>

std::pair<std::vector<Vertex>, int> readVertices(const char* filename)
{
	std::ifstream plik(filename);
	size_t N;
	size_t M;

	if (!plik.good()) {
		throw " Blad pliku!";
	}

	int budget;
	plik >> N;
	plik >> M;
	plik >> budget;

	std::vector<Vertex> vertices(N);

	for (size_t i = 0; i < N; i++) {
		plik >> vertices[i].max_value;
		plik >> vertices[i].min_value;
		plik >> vertices[i].change_cost;
		vertices[i].value = vertices[i].max_value;
	}

	int from, to;
	for (size_t i = 0; i < M; i++) {
		plik >> from;
		plik >> to;
		from -= 1;
		to -= 1;
		vertices[from].vertexesAfterThis.push_back(to);
		vertices[to].vertexesBeforeThisLeft++;

		vertices[from].vertexesAfterThisLeft++;
		vertices[to].vertexesBeforeThis.push_back(from);
	}
	return std::make_pair(vertices, budget);
}