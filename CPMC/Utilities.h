#pragma once
#include <vector>
#include "Vertex.h"

std::pair<std::vector<Vertex>, int> readVertices(const char* filename);
