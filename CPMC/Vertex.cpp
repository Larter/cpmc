#include "Vertex.h"
#include <iostream>
#include <iomanip>

void Vertex::display() const
{
	std::cout << "value: " << std::setw(5) << value << " ES:" << std::setw(5) << early_start << " EF:" <<
		std::setw(5) << early_finish << " LS:" << std::setw(5) << latest_start << " LF:" << std::setw(5) << latest_finish;
	std::cout << std::endl;
}

void Vertex::resetVertexCountingParameters()
{
	latest_finish = 0;
	latest_start = 0;
	early_start = 0;
	early_finish = 0;
	vertexesAfterThisLeft = vertexesAfterThis.size();
	vertexesBeforeThisLeft = vertexesBeforeThis.size();
	critical_path_value = 0;
}
