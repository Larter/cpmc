#pragma once
#include<vector>

struct Vertex
{
	Vertex()
		:value(0),
		vertexesBeforeThisLeft(0),
		early_start(0),
		vertexesAfterThisLeft(0),
		latest_finish(0),
		min_value(0),
		max_value(0),
		change_cost(0),
		critical_path_value(0)
	{}
	int critical_path_value;
	int value;
	int min_value;
	int max_value;
	int change_cost;
	int vertexesBeforeThisLeft;
	int vertexesAfterThisLeft;

	int early_start, early_finish, latest_start, latest_finish;
	std::vector<int> vertexesAfterThis;
	std::vector<int> vertexesBeforeThis;


	void display() const;
	void resetVertexCountingParameters();
};

