// CPMC.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include "CriticalPath.h"
#include "Vertex.h"
#include "Utilities.h"
#include "PSO.h"
#include "CPMC.h"




int main(int argc, char* argv[]) {

	if (argc != 2)
	{
		std::cerr << "Prosze podac tylko nazwe pliku jako argument" << std::endl;
		return 1;
	}
	
	try
	{
		auto readData = readVertices(argv[1]);

		FillCriticalPathsValues(readData.first);
		int cmax_original = getCMaxValueFromFilledData(readData.first);
		std::cout << "Wartosc przed zmianami " << cmax_original << std::endl;

		{
			std::cout << "Algorytm deterministyczny" << std::endl;

			auto res = CPMC(readData.first, readData.second);


			std::cout << "Wynik po cieciach to: " << res.first << std::endl;

			std::cout << "Uzyty budget to" << std::endl << res.second << std::endl;

			std::cout << "Zmienijszenie koszt�w" << std::endl;
			for (auto& vertex : readData.first)
			{
				std::cout << vertex.max_value - vertex.value << " ";
			}
		}
		for (auto& vertex : readData.first)
		{
			vertex.value = vertex.max_value;
		}

		{
			std::cout << std::endl << "Algorytm heurystyczny" << std::endl;

			auto res = PSOAlgorithm(readData.first, readData.second);


			std::cout << "Wynik po cieciach to: " << res.first << std::endl;

			std::cout << "Uzyty budget to" << std::endl << res.second << std::endl;

			std::cout << "Zmienijszenie koszt�w" << std::endl;
			for (auto& vertex : readData.first)
			{
				std::cout << vertex.max_value - vertex.value << " ";
			}
		}
		for (auto& vertex : readData.first)
		{
			vertex.value = vertex.max_value;
		}
		{
			std::cout << std::endl << "Algorytm deterministyczny 2" << std::endl;


			std::cout << "\\item " << argv[1] << std::endl << std::endl;
			std::cout << "no reduction scheduler time:" << cmax_original << std::endl << std::endl;
			auto res = CPMC2(readData.first, readData.second);


			std::cout << "reduction scheduler time: " << res.first << std::endl << std::endl;


			std::cout << "reduction cost: " << res.second << std::endl << std::endl;

			std::cout << "reduction:" << std::endl << std::endl;
			for (auto& vertex : readData.first)
			{
				std::cout << vertex.max_value - vertex.value << " ";
			}
		}
		std::cout << std::endl;
	}
	catch (...)
	{
		std::cout << "Wrong Filename" << std::endl;
	}
}

